resource "aws_iam_openid_connect_provider" "gitlab" {
  client_id_list  = [var.gitlab_url]

  thumbprint_list = [data.tls_certificate.gitlab.certificates.0.sha1_fingerprint]
  url             = var.gitlab_url
}