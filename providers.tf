provider "aws" {
  region = "us-east-1"
}

terraform {
  required_version = "1.6.2"

  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "5.4.0"
    }
  }
  backend "s3" {}
}