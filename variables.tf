variable "gitlab_group" {
  type = string
  description = "Gitlab group name"
  default = "aws_project1"
}

variable "gitlab_project" {
  type = string
  default = "terraform-oidc"
  description = "Gitlab project name"
}

variable "gitlab_branch" {
  type = string
  description = "Gitlab project branch name"
  default = "main"
}

variable "gitlab_url" {
    type = string
  description = "Gitlab url"
  default = "https://gitlab.com"
}

variable "role_name" {
  type = string
  description = "Gitlab iam role name"
  default = "gitlab-ci"
}